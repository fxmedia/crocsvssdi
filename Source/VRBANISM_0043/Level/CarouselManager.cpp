// Fill out your copyright notice in the Description page of Project Settings.


#include "CarouselManager.h"

// Sets default values
ACarouselManager::ACarouselManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACarouselManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACarouselManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACarouselManager::UpdateObject(FReplyLiveObjectData data)
{
	for (ACarouselObject* Object : Objects) {
		if (Object) {
			if (Object->Name == data.name) {
				Object->ShowCarouselObject();
			}
			else {
				Object->HideCarouselObject();
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("ACarouselManager::UpdateObject No Object selected"));
		}
	}
	
}