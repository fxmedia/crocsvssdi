// Fill out your copyright notice in the Description page of Project Settings.


#include "DownloadManager.h"

// Sets default values
ADownloadManager::ADownloadManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADownloadManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADownloadManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Current downloads = %d"), CurrentDownloads);
	//If we are not performing the max amount of downloads
	//if (CurrentDownloads < MaxDownloads) {
	//	UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Current InviteeDownloadList = %d"), InviteeDownloadList.Num());
		// If we have at least 1 item in the download list
	//	if (InviteeDownloadList.Num() > 0) {

			// Get the 0th element from the InviteeDownloadList
	//		AInvitee* DownloadInvitee = InviteeDownloadList[0];
	//		UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Using invitee with id = %d"), DownloadInvitee->InviteeData.id);
			
			// Remove the 0th element from the InviteeDownloadlist
	//		InviteeDownloadList.RemoveAt(0);
	//		UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Removed item from inviteedownloadlist, remaining length = %d"), InviteeDownloadList.Num());
			
			// Start listening to the finished download delegate of the Invitee
	//		DownloadInvitee->FinishedDownloadingAvatar.AddDynamic(this, &ADownloadManager::FinishDownload);

			// Start the next download
	//		StartDownload(DownloadInvitee);
	//	}
	//}

	// Do a new download every interval, determined by DownloadEveryS
	KeepSeconds += DeltaTime;
	if (KeepSeconds > DownloadEveryS) {
		KeepSeconds = 0.0f;
		if (InviteeDownloadList.Num() > 0) {

			// Get the 0th element from the InviteeDownloadList
			AInvitee* DownloadInvitee = InviteeDownloadList[0];
			//UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Using invitee with id = %d"), DownloadInvitee->InviteeData.id);

			// Remove the 0th element from the InviteeDownloadlist
			InviteeDownloadList.RemoveAt(0);
			//UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Removed item from inviteedownloadlist, remaining length = %d"), InviteeDownloadList.Num());

			// Start listening to the finished download delegate of the Invitee
			//DownloadInvitee->FinishedDownloadingAvatar.AddDynamic(this, &ADownloadManager::FinishDownload);

			// Start the next download
			StartDownload(DownloadInvitee);
		}
	}

}

void ADownloadManager::StartDownload(AInvitee* invitee)
{
	UE_LOG(LogTemp, Log, TEXT("ADownloadManager::StartDownload Executing"));
	UE_LOG(LogTemp, Log, TEXT("ADownloadManager::Tick Current downloads = %d"), CurrentDownloads);
	// Start the download
	invitee->BPSetAvatar(invitee->InviteeData.avatar);

	// Increment the current downloads up
	CurrentDownloads += 1;

	// How do we know when the download is finished?
}

void ADownloadManager::FinishDownload()
{
	UE_LOG(LogTemp, Log, TEXT("ADownloadManager::FinishDownload Executing"));
	
	// Increment the current downloads down
	CurrentDownloads -= 1;
}

void ADownloadManager::AddDownload(AInvitee* invitee)
{
	UE_LOG(LogTemp, Log, TEXT("ADownloadManager::AddDownload Executing"));
	InviteeDownloadList.Add(invitee);
}
